import hibpwned
import sys
from time import sleep
import argparse

def displayBreaches(given_email, output_file, disp_cc_info):
    f = None
    if output_file:
        f = open(output_file, 'a')

    myApp = hibpwned.Pwned(given_email, 'Chris App', 'c04baeca91ef49c6973cb87a778db562')

    myBreaches = myApp.searchAllBreaches()

    if type(myBreaches) == list:
        print('**'+given_email, ":", len(myBreaches))

        if f:
            f.write('**'+given_email+' : '+str(len(myBreaches))+'\n')

        for breach in myBreaches:
            print(breach['Title'])
            print(breach['BreachDate'])
            print("IsVerified="+str(breach['IsVerified']))
            print('Data Classes: '+str(breach['DataClasses']))
            print()

            if output_file:
                f.write(breach['Title']+'\n')
                f.write(breach['BreachDate']+'\n')
                f.write('IsVerified='+str(breach['IsVerified'])+'\n')
                f.write('Data Classes: '+str(breach['DataClasses'])+'\n')
                f.write('\n')

    print()

    if f:
        f.write('\n')
        f.close()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='HaveIBeenPwned api script.')
    parser.add_argument('emailFile', help='newline separated list of emails to check')
    parser.add_argument('-o', help='desired output file for breach info')
    parser.add_argument('-t', help='time in ms between API calls')
    
    args = parser.parse_args()

    rest_time = 1000
    disp_cc_info = False
    output_file = False

    if args.t:
        rest_time = args.t
        
    if args.o:
        output_file = args.o

    content = []

    with open(sys.argv[1]) as f:
        content = f.readlines()

    content = [x.strip() for x in content]

    for email in content:
        displayBreaches(email, output_file, disp_cc_info)

        #need to wait between calls
        sleep(float(rest_time) * 0.001)
